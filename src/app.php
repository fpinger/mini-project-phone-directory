<?php

try {

    error_reporting(E_ALL);
    ini_set('display_errors', '0');
    define("TIMEZONE", "Asia/Vladivostok");
    date_default_timezone_set(TIMEZONE); 

    require __DIR__ . "/functions.php";
    
    $contacts = loadStorage();
    $errors = [];
    $full_name = "";
    $phone = "";

    if (isPostRequest()) {
        $action = postValue("action", "");
        switch ($action) {
            case "append_contact":
                $full_name = postValue("full_name", "");
                $phone = postValue("phone", "");

                $errors = validateContact($full_name, $phone);
                if (!$errors) {
                    $contacts[] = [
                        "contact_id" => uniqid(),
                        "created_at" => time(),
                        "full_name"  => $full_name,
                        "phone"      => $phone,
                    ];
                    updateStorage($contacts);
                    redirect("/");
                }
                break;
            case "remove_contact":
                $contact_id = postValue("contact_id", "");
                $updated_contacts = array_filter($contacts, function($contact) use ($contact_id) {
                    return ($contact["contact_id"] !== $contact_id);
                });

                updateStorage($updated_contacts);
                redirect("/");
                break;
            default:
                returnResponse("Not Acceptable", 406);
        }
    }

    if ($errors) {
        http_response_code(422);
    }

    $order_field = requestValue("field", "");
    $order_direction = requestValue("direction", "");
    $contacts = orderContacts($contacts, $order_field, $order_direction);

    /*
    * В шаблоне используются:
    * $contacts        - array
    * $errors          - array
    * $full_name       - string
    * $phone           - string
    * $order_field     - string
    * $order_direction - string
    */
    require __DIR__ . "/view.php";

} catch (Throwable $t) {
    http_response_code(500);
    die("Internal Server Error");
}
