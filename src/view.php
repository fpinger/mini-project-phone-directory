<?php

?><!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Телефонный справочник</title>
    <link rel="stylesheet" href="/css/styles.css">
</head>
<body>
    <main>
        <h1>Телефонный справочник</h1>

        <section class="append-contact-section">

            <h2>Добавление контакта</h2>

            <form method="post" class="append-contact-form">
                <input type="hidden" name="action" value="append_contact">
                
                <label>Имя</label>
                <input class="input-fields" type="text" name="full_name" value="<?=e($full_name)?>">
                <?php if (isset($errors["full_name"])): ?>
                    <div class="form-error alert-danger">
                        <?=$errors["full_name"]?>
                    </div>
                <?php endif; ?>

                <label>Телефон</label>
                <input class="input-fields" type="text" name="phone" value="<?=e($phone)?>">
                <?php if (isset($errors["phone"])): ?>
                    <div class="form-error alert-danger">
                        <?=$errors["phone"]?>
                    </div>
                <?php endif; ?>
                
                <button type="submit" class="btn append-contact-btn">Добавить</button>
            </form>

        </section>

        <section class="contacts-list-section" id="contactsListSection">

            <h2>Список контактов</h2>

            <?php if (empty($contacts)): ?>
                <div class="alert-worning">Нет ни одного контакта</div>
            <?php else: ?>
                <table>
                    <thead>
                        <tr>
                            <th>
                                Имя
                                <?php if (currentOrder("full_name", "asc", $order_field, $order_direction)): ?>
                                    <b>↑</b>
                                <?php else: ?>
                                    <a href="?field=full_name&direction=asc">↑</a>
                                <?php endif; ?>

                                <?php if (currentOrder("full_name", "desc", $order_field, $order_direction)): ?>
                                    <b>↓</b>
                                <?php else: ?>
                                    <a href="?field=full_name&direction=desc">↓</a>
                                <?php endif; ?>
                            </th>
                            <th>
                                Телефон
                                <?php if (currentOrder("phone", "asc", $order_field, $order_direction)): ?>
                                    <b>↑</b>
                                <?php else: ?>
                                    <a href="?field=phone&direction=asc">↑</a>
                                <?php endif; ?>

                                <?php if (currentOrder("phone", "desc", $order_field, $order_direction)): ?>
                                    <b>↓</b>
                                <?php else: ?>
                                    <a href="?field=phone&direction=desc">↓</a>
                                <?php endif; ?>
                            </th>
                            <th class="remove-contact-form-col">Удалить</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($contacts as $contact): ?>
                        <tr>
                            <td>
                                <?=e($contact["full_name"])?>
                            </td>
                            <td>
                                <?=e($contact["phone"])?>
                            </td>
                            <td>
                                <form method="post" class="remove-contact-form">
                                    <input type="hidden" name="action" value="remove_contact">
                                    <input type="hidden" name="contact_id" value="<?=$contact["contact_id"]?>">
                                    <button type="submit" class="btn remove-contact-btn">Удалить</button>
                                </form>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </section>
    </main>

    <script>
        const contactsListSection = document.getElementById("contactsListSection");
        const removeContactHandler = function (ev) {
            if (ev.target.classList.contains('remove-contact-btn') && 
                !confirm("Вы действительно хотите удалить контакт?")) {
                ev.preventDefault();
            }
        };
        contactsListSection.addEventListener('click', removeContactHandler);
        window.onunload = function() {
            contactsListSection.removeEventListener('click', removeContactHandler);
            return;
        }
    </script>
</body>
</html>