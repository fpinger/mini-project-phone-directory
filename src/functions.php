<?php

define("CONTACTS_STORAGE", __DIR__ . "/storage.json");
define("PHONE_MIN_LENGTH", 10);

function redirect($uri)
{
    header("Location: {$uri}", true, 302);
    exit();
}

function returnResponse($content, $code=200)
{
    http_response_code($code);
    echo $content;
    exit();
}

function requestValue($name, $default=null)
{
    return isset($_REQUEST[$name]) ? $_REQUEST[$name]: $default;
}

function postValue($name, $default=null)
{
    return isset($_POST[$name]) ? $_POST[$name]: $default;
}

function isPostRequest()
{
    return $_SERVER["REQUEST_METHOD"] === "POST";
}

function initStorage()
{
    updateStorage([]);
}

function loadStorage()
{
    if (!is_file(CONTACTS_STORAGE)) {
        initStorage();
    }
    $json = file_get_contents(CONTACTS_STORAGE);
    return json_decode($json, true);
}

function updateStorage($array)
{
    $json = json_encode($array);
    file_put_contents(CONTACTS_STORAGE, $json);
}

function e($value)
{
    return htmlspecialchars($value);
}

function isValidFullName($fullName)
{
    $full_name = trim($fullName);
    return !empty($full_name);
}

function isValidPhone($phone)
{
    $cleared_phone = preg_replace('/[^0-9]/', '', $phone);
    return !(empty($phone) || strlen($phone) < PHONE_MIN_LENGTH);
}

function validateContact($fullName, $phone)
{
    $errors = [];

    if (!isValidFullName($fullName)) {
        $errors["full_name"] = "Имя не может быть пустым.";
    }
    if (!isValidPhone($phone)) {
        $errors["phone"] = "Телефон должен содержать минимум " . PHONE_MIN_LENGTH . " цифр.";
    }
    return $errors;
}

function createdAtDesc($a, $b) {
    return $a["created_at"] <= $b["created_at"]; 
} 

function orderContacts($contacts, $orderField, $orderDirection)
{
    $order_fields = ["full_name", "phone"];
    $order_directions = ["asc", "desc"];
    if (in_array($orderField, $order_fields) 
        && in_array($orderDirection, $order_directions)) {

        usort($contacts, function ($c1, $c2) use ($orderField, $orderDirection) {
            if ($orderDirection === "asc") {
                return strnatcmp($c1[$orderField], $c2[$orderField]);
            }
            return strnatcmp($c2[$orderField], $c1[$orderField]);
        });
    } else {
        usort($contacts, function ($c1, $c2) {
            return strnatcmp($c2["created_at"], $c1["created_at"]); 
        });
    }
    return $contacts;
}

function currentOrder($orderField, $orderDirection, $currentOrderField, $currentOrderDirection)
{
    return ($orderField === $currentOrderField && $orderDirection === $currentOrderDirection);
}